# x11glove

![](https://img.shields.io/badge/written%20in-C%2B%2B%20%28Qt%29-blue)

A graphical X11 inspector.

Browse and manipulate the X11 window hierarchy.

## Changelog

2017-07-08 0.1
- Initial public release
- [⬇️ x11glove-src-0.1.tar.gz](dist-archive/x11glove-src-0.1.tar.gz) *(6.23 KiB)*

